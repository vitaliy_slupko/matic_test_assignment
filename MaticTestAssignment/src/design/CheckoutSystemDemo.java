package design;

import design.checkoutsystem.Checkout;
import design.checkoutsystem.rules.BuyOneGetOneFreeCheckoutRuleImpl;
import design.checkoutsystem.rules.MinItemsCountDiscountCheckoutRuleImpl;

import java.util.Arrays;

/**
 * @author Vitaliy Slupko
 */
public class CheckoutSystemDemo {

    public static void main (final String[] args) {

        final String[] itemsToScan = new String[] {
                CurrentInventory.FRUIT_TEA_CODE,
                CurrentInventory.STRAWBERRIES_CODE,
                CurrentInventory.FRUIT_TEA_CODE,
                CurrentInventory.FRUIT_TEA_CODE,
                CurrentInventory.COFFEE_CODE
        };

//        final String[] itemsToScan = new String[] {
//                CurrentInventory.FRUIT_TEA_CODE,
//                CurrentInventory.FRUIT_TEA_CODE
//        };

//        final String[] itemsToScan = new String[] {
//                CurrentInventory.STRAWBERRIES_CODE,
//                CurrentInventory.STRAWBERRIES_CODE,
//                CurrentInventory.FRUIT_TEA_CODE,
//                CurrentInventory.STRAWBERRIES_CODE
//        };

        final Checkout checkout = new Checkout(
                new CurrentInventory(),
                new BuyOneGetOneFreeCheckoutRuleImpl(CurrentInventory.FRUIT_TEA_CODE),
                new MinItemsCountDiscountCheckoutRuleImpl(CurrentInventory.STRAWBERRIES_CODE, 3, 4.50)
        );

        for (String itemCode : itemsToScan) {
            checkout.scan(itemCode);
        }

        System.out.println("Items: " + Arrays.toString(itemsToScan) + "; total = " + checkout.getTotal());
    }
}
