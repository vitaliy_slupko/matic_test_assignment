package design;

import design.checkoutsystem.Inventory;
import design.checkoutsystem.Item;

/**
 * @author Vitaliy Slupko
 */
public class CurrentInventory extends Inventory {

    public static final String FRUIT_TEA_CODE = "FR";
    public static final String STRAWBERRIES_CODE = "SR";
    public static final String COFFEE_CODE = "CF";
    public static final String APPLE_JUICE = "AJ";

    public CurrentInventory() {
        super(
                new Item(FRUIT_TEA_CODE, "Fruit Tea", 3.11),
                new Item(STRAWBERRIES_CODE, "Strawberries", 5.0),
                new Item(COFFEE_CODE, "Coffee", 11.23),
                new Item(APPLE_JUICE, "Apple Juice", 7.25)
        );
    }
}
