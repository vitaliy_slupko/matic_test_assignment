package design.checkoutsystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vitaliy Slupko
 */
public class Checkout {

    private final Inventory inventory;
    private final ICheckoutRule[] rules;

    private final List<Item> scannedItems = new ArrayList<>();

    public Checkout(final Inventory inventory, final ICheckoutRule... rules) throws IllegalArgumentException {
        if (null == inventory) {
            throw new IllegalArgumentException("Inventory cannot be null");
        }
        this.inventory = inventory;
        this.rules = rules;
    }

    /**
     * Scans given item code.
     *
     * @param itemCode item code to scan
     * @return {@code true} if given item code is recognized successfully, otherwise returns {@code false}
     */
    public boolean scan(final String itemCode) {
        final Item item = inventory.getItem(itemCode);
        if (null != item) {
            scannedItems.add(item);
            return true;
        }
        return false;
    }

    /**
     * @return total price of this checkout
     */
    public double getTotal() {
        /*
        Copy items in order to persist "clean" state of scanned items.
        Just in case new items are scanned after total is returned or there will be requirement to change rules dynamically etc.
         */
        final List<Item> itemsToProcess = new ArrayList<>();
        for (Item item : scannedItems) {
            itemsToProcess.add(new Item(item.getCode(), item.getName(), item.getPrice()));
        }
        if (null != rules) {
            for (ICheckoutRule rule : rules) {
            /*
            Null check... just in case...
             */
                if (null != rule) {
                    rule.processItems(itemsToProcess);
                }
            }
        }
        double total = 0.0;
        for (Item item : itemsToProcess) {
            total += item.getPrice();
        }
        return total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Checkout checkout = (Checkout) o;

        if (inventory != null ? !inventory.equals(checkout.inventory) : checkout.inventory != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(rules, checkout.rules)) return false;
        return scannedItems != null ? scannedItems.equals(checkout.scannedItems) : checkout.scannedItems == null;

    }

    @Override
    public int hashCode() {
        int result = inventory != null ? inventory.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(rules);
        result = 31 * result + (scannedItems != null ? scannedItems.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Checkout{" +
                "inventory=" + inventory +
                ", rules=" + Arrays.toString(rules) +
                ", scannedItems=" + scannedItems +
                '}';
    }
}
