package design.checkoutsystem;

/**
 * @author Vitaliy Slupko
 */
public class Item {

    private final String code;
    private final String name;
    private double price;

    public Item(final String code, final String name, final double price) throws IllegalArgumentException {
        if ((null == code) || code.isEmpty()) {
            throw new IllegalArgumentException("Code cannot be null or empty string; code = " + code);
        }
        if ((null == name) || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be null or empty string; name = " + name);
        }
        this.code = code;
        this.name = name;
        setPrice(price);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(final double price) {
        this.price = (0.0 > price) ? 0.0 : price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (Double.compare(item.price, price) != 0) return false;
        if (code != null ? !code.equals(item.code) : item.code != null) return false;
        return name != null ? name.equals(item.name) : item.name == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
