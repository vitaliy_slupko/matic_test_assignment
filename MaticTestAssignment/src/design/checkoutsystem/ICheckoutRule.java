package design.checkoutsystem;

import java.util.List;

/**
 * @author Vitaliy Slupko
 */
public interface ICheckoutRule {

    /**
     * Processes given items.
     *
     * @param items
     */
    void processItems(List<Item> items);
}
