package design.checkoutsystem.rules;

import design.checkoutsystem.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vitaliy Slupko
 */
public class MinItemsCountDiscountCheckoutRuleImpl extends AbstractCheckoutRule {

    private final int minItemsCount;
    private final double discountPrice;

    public MinItemsCountDiscountCheckoutRuleImpl(
            final String itemCode,
            final int minItemsCount,
            final double discountPrice) throws IllegalArgumentException {
        super(itemCode);
        if (1 > minItemsCount) {
            throw new IllegalArgumentException("Minimum items count cannot be less than 1; minItemsCount = " + minItemsCount);
        }
        if (0.0 > discountPrice) {
            throw new IllegalArgumentException("Discount price must be positive number or 0; discountPrice = " + discountPrice);
        }
        this.minItemsCount = minItemsCount;
        this.discountPrice = discountPrice;
    }

    @Override
    public void processItems(final List<Item> items) {
        if ((null == items) || items.isEmpty()) {
            return;
        }
        final String itemCode = getItemCode();
        List<Item> matchingItems = null;
        for (Item item : items) {
            if (itemCode.equals(item.getCode())) {
                if (null == matchingItems) {
                    matchingItems = new ArrayList<>();
                }
                matchingItems.add(item);
            }
        }

        if (null == matchingItems) {
            return;
        }

        final boolean applyDiscount = (minItemsCount <= matchingItems.size());
        if (applyDiscount) {
            for (Item item : matchingItems) {
                item.setPrice(discountPrice);
            }
        }
    }
}
