package design.checkoutsystem.rules;

import design.checkoutsystem.Item;

import java.util.List;

/**
 * Vitaliy Slupko
 */
public class BuyOneGetOneFreeCheckoutRuleImpl extends AbstractCheckoutRule {

    public BuyOneGetOneFreeCheckoutRuleImpl(final String itemCode) throws IllegalArgumentException {
        super(itemCode);
    }

    @Override
    public void processItems(final List<Item> items) {
        if ((null == items) || items.isEmpty()) {
            return;
        }
        final String itemCode = getItemCode();
        int fruitTeaItemsCount = 0;
        for (Item item : items) {
            if (itemCode.equals(item.getCode())) {
                fruitTeaItemsCount++;
                if ((fruitTeaItemsCount % 2) == 0) {
                    item.setPrice(0.0);
                }
            }
        }
    }
}
