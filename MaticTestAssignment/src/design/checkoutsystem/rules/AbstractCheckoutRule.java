package design.checkoutsystem.rules;

import design.checkoutsystem.ICheckoutRule;

/**
 * @author Vitaliy Slupko
 */
public abstract class AbstractCheckoutRule implements ICheckoutRule {

    private final String itemCode;

    public AbstractCheckoutRule(final String itemCode) throws IllegalArgumentException {
        if ((null == itemCode) || itemCode.isEmpty()) {
            throw new IllegalArgumentException("Item code cannot be null or empty");
        }
        this.itemCode = itemCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractCheckoutRule that = (AbstractCheckoutRule) o;

        return itemCode != null ? itemCode.equals(that.itemCode) : that.itemCode == null;

    }

    @Override
    public int hashCode() {
        return itemCode != null ? itemCode.hashCode() : 0;
    }
}
