package design.checkoutsystem;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vitaliy Slupko
 */
public class Inventory {

    private final Map<String, Item> inventoryState = new HashMap<>();

    public Inventory(final Item... items) throws IllegalArgumentException {
        if ((null == items) || (0 == items.length)) {
            throw new IllegalArgumentException("Items cannot be null and there should be at least one item; items = " + items);
        }
        for (Item item : items) {
            inventoryState.put(item.getCode(), item);
        }
    }

    /**
     * Returns item for given code.
     *
     * @param itemCode
     * @return item that corresponds to the given code or {@code null} if there is no item for given code
     */
    public Item getItem(final String itemCode) {
        final Item item = inventoryState.get(itemCode);
        if (null != item) {
            return new Item(item.getCode(), item.getName(), item.getPrice());
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inventory inventory = (Inventory) o;

        return inventoryState != null ? inventoryState.equals(inventory.inventoryState) : inventory.inventoryState == null;

    }

    @Override
    public int hashCode() {
        return inventoryState != null ? inventoryState.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "inventoryState=" + inventoryState +
                '}';
    }
}
