package algorithms;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Vitaliy Slupko
 */
public class Combinations {

    public static void main(final String[] args) {
        final String[] dictionary = new String[] {"a", "b", "c", "ab", "abc"};
        final String target = "aabc";

        System.out.println("findCombinations(" + Arrays.toString(dictionary) + ", " + target + ") = " + findCombinations(dictionary, target));
    }

    /**
     * Returns combinations of strings from dictionary that match target string.
     * <p>
     * <b>NOTE:</b> assumes that dictionary does not contain duplications.
     *
     * @param dictionary strings dictionary
     * @param target     target string
     * @return combinations of strings from dictionary that match target string
     */
    private static Set<Combination> findCombinations(final String[] dictionary, final String target) {
        final Set<Combination> combinations = new LinkedHashSet<>();
        final Set<Combination> intermediateCombinations = new LinkedHashSet<>();
        for (String str : dictionary) {
            if (target.contains(str)) {
                processCombinations(new Combination(str), intermediateCombinations, combinations, target);
            }
        }
        return combinations;
    }

    private static void processCombinations(
            final Combination newCombination,
            final Set<Combination> intermediateCombinations,
            final Set<Combination> matchingCombinations,
            final String target) {
        final String combinationStr = newCombination.getCombinationString();
        if (target.matches(combinationStr)) {
            matchingCombinations.add(newCombination);
            return;
        }
        if (target.contains(combinationStr)) {
            intermediateCombinations.add(newCombination);
        } else {
            return;
        }
        /*
        Try to build new combinations using intermediate combinations.
         */
        final Set<Combination> intermediateCombinationsCopy = new LinkedHashSet<>(intermediateCombinations);
        for (Combination intermediateCombination : intermediateCombinationsCopy) {
            final  Combination newCombinationFirst = new Combination(newCombination, intermediateCombination);
            processCombinations(newCombinationFirst, intermediateCombinations, matchingCombinations, target);

            final Combination newCombinationSecond = new Combination(intermediateCombination, newCombination);
            processCombinations(newCombinationSecond, intermediateCombinations, matchingCombinations, target);
        }
    }

    /**
     * Represents single combination which is build from separate strings taken from dictionary.
     */
    private static class Combination {

        private final String[] strings;

        private String combinationString;

        private Combination(String... strings) {
            if ((null == strings) || (0 == strings.length)) {
                throw new IllegalArgumentException("Combination cannot be empty");
            }
            this.strings = strings;
        }

        private Combination(final Combination combination1, final Combination combination2) {
            if (null == combination1) {
                throw new IllegalArgumentException("Combination 1 cannot be null");
            }
            if (null == combination2) {
                throw new IllegalArgumentException("Combination 2 cannot be null");
            }
            strings = new String[combination1.strings.length + combination2.strings.length];
            for (int i = 0; i < combination1.strings.length; i++) {
                strings[i] = combination1.strings[i];
            }
            for (int i = 0; i < combination2.strings.length; i++) {
                strings[i + combination1.strings.length] = combination2.strings[i];
            }
        }

        public String getCombinationString() {
            if (null == combinationString) {
                final StringBuilder combinationStringBuilder = new StringBuilder();
                for (String string : strings) {
                    combinationStringBuilder.append(string);
                }
                combinationString = combinationStringBuilder.toString();
            }
            return combinationString;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Combination that = (Combination) o;

            // Probably incorrect - comparing Object[] arrays with Arrays.equals
            return Arrays.equals(strings, that.strings);

        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(strings);
        }

        @Override
        public String toString() {
            return "Combination{" +
                    "strings=" + Arrays.toString(strings) +
                    '}';
        }
    }
}
